FROM node:current-alpine3.12

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

#Dependencies for yarn test. Alpine image does not have glibc by default
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
 && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.32-r0/glibc-2.32-r0.apk \
 && apk --no-cache add \
    glibc-2.32-r0.apk 

COPY . /app

RUN yarn test && yarn build

CMD yarn start

EXPOSE 3000
